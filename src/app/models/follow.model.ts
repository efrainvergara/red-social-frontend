/**
  * Propiedad BOSOCIAL 2020
*/

export class Follow {
    constructor(
        public _id:string,
        public user:string,
        public followed:string,
    ){}
}
