/**
  * Propiedad BOSOCIAL 2020
*/

export class Publication {
    constructor(
        public _id: string,
        public text: string,
        public file: string,
        public created_at: string,
        public user: string,
        public userOwn: string,
        public likes: any,
        public likedBy: any[],
        public comments: [{
            comment: string,
            commentatorId: string,
            created_at: string
        }],
        public isShared: Boolean,
    ) { }
}
