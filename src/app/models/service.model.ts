/**
  * Propiedad BOSOCIAL 2020
*/

export class Service {
    constructor(
        public _id:string,
        public title:string,
        public description:string,
        public date:string,
        public hour:string,
        public cost:string,
        public accept:string,
        public backgroundColor:string,
        public created_at:string,
        public userContratista:string,
        public userContratante:any,
    ){}
}