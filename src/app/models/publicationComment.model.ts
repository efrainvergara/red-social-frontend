/**
  * Propiedad BOSOCIAL 2020
*/

export class PublicationComment {
    constructor(
        public _id:string,
        public comment:string,
        public created_at:string,
        public publication:string,
        public user:string
    ){}
}
