/**
  * Propiedad BOSOCIAL 2020
*/

export class User {
    constructor(
        public _id:string,
        public name:string,
        public surname:string,
        public email:string,
        public t_identificacion:string,
        public n_identificacion:string,
        public nick:string,
        public tipo_servicio:string,
        public servicios:string,
        public ciudad:string,
        public localidad:string,
        public web:string,
        public password:string,
        public currentPassword:string,
        public newPassword:string,
        public verifyPassword:string,
        public role:string,
        public image:string,
        public portada:string,
        public telefono:string,
        public facebook:string,
        public twitter:string
    ){}
}