/**
  * Propiedad BOSOCIAL 2020
*/

import { Component, OnInit } from '@angular/core';
import { UserService } from './../../services/user.service'
import { ActivatedRoute, Router } from '@angular/router';
import { GLOBAL } from 'src/app/services/global';
/* jQUERY */
declare var jQuery: any;
declare var $: any;

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css'],
  providers: [UserService]
})
export class SearchComponent implements OnInit {
  results: any[];
  status: string;
  numberItems: any;
  word: any;
  url: string;

  constructor(
    private _userService: UserService,
    private _route: ActivatedRoute,
    private _router: Router
  ) {
    this.url = GLOBAL.url;
  }

  ngOnInit(): void {
    this.loadPage();

    let state = $('#filter');
    let city = $('#filter2');

    state.empty();
    city.empty();

    state.append('<option value="" disabled selected hidden>Filtrar</option>');
    state.prop('selectedIndex', 0);

    city.append('<option value="" disabled selected hidden>Filtrar</option>');
    city.prop('selectedIndex', 0);

    // Populate dropdown with list of provinces
    $.getJSON('assets/dataColombia.json', function (data) {
      $.each(data, function (key, entry) {
        state.append($('<option></option>').attr('value', key).text(key));
      })
      $("select.country").change(function () {
        var selectCiudad = $(this).children("option:selected").val();
        city.empty();
        $.each(data[selectCiudad], function (key, entry) {
          city.append($('<option></option>').attr('value', entry).text(entry));
        })
      });
    });

    //Filter function
    window.onload = function () {
      $("#filter").click(function () {

        var filter = $(this).val(),
          count = 0;

        $('#results article').each(function () {

          if ($(this).text().search(new RegExp(filter, "i")) < 0) {
            $(this).hide();

          } else {
            $(this).show();
            count++;
          }
        });
      });

      $("#filter2").click(function () {

        var filter = $(this).val(),
          count = 0;

        $('#results article').each(function () {

          if ($(this).text().search(new RegExp(filter, "i")) < 0) {
            $(this).hide();

          } else {
            $(this).show();
            count++;
          }
        });
      });

      $("#filter3").click(function () {

        var filter = $(this).val(),
          count = 0;

        $('#results article').each(function () {

          if ($(this).text().search(new RegExp(filter, "i")) < 0) {
            $(this).hide();

          } else {
            $(this).show();
            count++;
          }
        });
      });
    }

  }

  loadPage() {
    this._route.params.subscribe(params => {
      let id = params['id'];
      this.getSearch(id);
    })
  }

  getSearch(param) {
    this._userService.searchGlobal(param).subscribe(
      response => {
        this.word = response.word;
        this.numberItems = response.count;
        this.results = response.results;
      },
      error => {
        var errorMessage = <any>error;
        console.log(errorMessage);

        if (errorMessage != null) {
          this.status = 'error';
        }
      }
    )

  }

}
