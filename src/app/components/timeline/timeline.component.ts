/**
  * Propiedad BOSOCIAL 2020
*/

import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { UserService } from '../../services/user.service';
import { GLOBAL } from '../../services/global';
import { PublicationService } from '../../services/publication.service';
import { PublicationCommentService } from '../../services/publicationComment.service';
import { Publication } from '../../models/publication.model';
import { PublicationComment } from '../../models/publicationComment.model';
import { FormBuilder, Validators } from '@angular/forms';
import { ServiceService } from '../../services/service.service';
import { UploadService } from '../../services/upload.service';

/* jQUERY */
declare var jQuery: any;
declare var $: any;

@Component({
  selector: 'timeline',
  templateUrl: './timeline.component.html',
  styleUrls: ['./timeline.component.css'],
  providers: [UserService, PublicationService, PublicationCommentService, ServiceService, UploadService]
})
export class TimelineComponent implements OnInit {

  identity;
  token;
  title: string;
  url: string;
  status: string;
  page;
  total;
  pages;
  itemsPerPage;
  publications: Publication[];
  showImage;
  currentDate: any;
  publicationComment: PublicationComment;
  p: Publication;
  playerName: any;
  commentForm: any;
  providers: any[];
  publication: Publication;
  count: any;

  constructor(
    private _route: ActivatedRoute,
    private _router: Router,
    private _userService: UserService,
    private _publicationService: PublicationService,
    private _publicationCommentService: PublicationCommentService,
    private formBuilder: FormBuilder,
    private _serviceService: ServiceService,
    private _uploadService: UploadService,
  ) {
    this.title = 'Timeline';
    this.identity = this._userService.getIdentity();
    this.token = this._userService.getToken();
    this.url = GLOBAL.url;
    this.currentDate = Date();
    this.publicationComment = new PublicationComment("", "", "", "", this.identity._id);
    this.p = new Publication("", "", "", "", this.identity._id, "","", [], [{ 'comment': '', 'commentatorId': '', 'created_at': '' }], false);
    this.publication = new Publication("", "", "", "", this.identity._id, "","",[], [{'comment': '', 'commentatorId': '', 'created_at': ''}], true);  
  }

  ngOnInit(): void {
    console.log('timeline.component cargado correctamente')
    this.getPublications(this.page)
    this.createCommentForm();
    this.providerService();
  }
  
  getPublications(page, adding = false) {
    this._publicationService.getPublications(this.token, page).subscribe(
      response => {
        this.count = response.comments
        if (response.publications) {
          this.total = response.total_items;
          this.pages = response.pages;
          this.itemsPerPage = response.items_per_page;

          if (!adding) {
            this.publications = response.publications;
          } else {
            var arrayA = this.publications;
            var arrayB = response.publications;

            this.publications = arrayA.concat(arrayB);

            $("html, body").animate({ scrollTop: $('body').prop("scrollHeight") }, 500);
          }

          /*if(page > this.pages){
            this._router.navigate(['/home']);
          }*/
        } else {
          this.status = 'error'
        }
      },
      error => {
        var errorMessage = <any>error;
        console.log(errorMessage);

        if (errorMessage != null) {
          this.status = 'error';
        }
      }
    )
  }

  public noMore = false;

  viewMore() {
    this.page += 1;
    if (this.page == this.pages) {
      this.noMore = true;
    }

    this.getPublications(this.page, true);
  }

  refresh(event) {
    this.getPublications(1);
  }

  showThisImage(id) {
    this.showImage = id;
  }

  hideThisImage(id) {
    this.showImage = 0;
  }

  deletePublication(id) {
    this._publicationService.deletePublication(this.token, id).subscribe(
      response => {
        this.refresh(event);
      },
      error => {
        console.log(<any>error);
      }
    );
  }

  logout() {
    localStorage.clear();
    this.identity = null;
    this._router.navigate(['/login']);
  }

  likePublication(id) {
    this._publicationService.likePublication(this.token, id).subscribe(
      response => {
        this.refresh(event);
      },
      error => {
        console.log(<any>error);
      });
  }

  // Create form for posting comments
  createCommentForm() {
    this.commentForm = this.formBuilder.group({
      comment: ['', Validators.compose([
        Validators.required,
        Validators.minLength(1),
        Validators.maxLength(200)
      ])]
    })
  }

  // Function to post a new comment
  postComment(id) {
    const comment = this.commentForm.get('comment').value; // Get the comment value to pass to service function
    // Function to save the comment to the database
    this._publicationService.commentPublication(this.token, id, comment).subscribe(data => {
      this.getPublications(1); // Refresh all blogs to reflect the new comment
      this.commentForm.reset();
      this.status = 'success';
    }, error => {
      var errorMessage = <any>error;
      console.log(errorMessage);

      if (errorMessage != null) {
        this.status = 'error';
      }
    });
  }

  //output
  @Output() sended = new EventEmitter();
  sendPublication(event) {
    this.sended.emit({ send: 'true' });
  }

  providerService() {
    this._serviceService.getProviderServices(this.token).subscribe(
      response => {
        //permite que se pinte la data
        this.providers = response.providers;
      },
      error => {
        var errorMessage = <any>error;
        console.log(errorMessage);

        if (errorMessage != null) {
          this.status = 'error';
        }
      }
    )
  }

  deleteComment(publication, comment) {
    this._publicationService.deleteComment(this.token, publication, comment).subscribe(
      response => {
        this.refresh(event);
      },
      error => {
        console.log(<any>error);
      }
    );
  }

  updatePublicationSubmit(form, id) {
    this._publicationService.updatePublication(id, this.publication, this.token).subscribe(
      response => {
      },
      error => {
        console.log(<any>error);
      }
    );
  }

  onSubmit(form) {
    this._publicationService.addPublication(this.token, this.publication).subscribe(
      response => {
        if (response.publication) {
          //this.publication = response.publication;
          if (this.filesToUpload && this.filesToUpload.length) {
            //Subir imagen
            this._uploadService.makeFileRequest(this.url + 'upload-image-pub/' + response.publication._id, [], this.filesToUpload, this.token, 'image')
              .then((result: any) => {
                this.publication.file = result.image;
                this.status = 'success';
                form.reset();
                this._router.navigate(['/']);
                this.sended.emit({ send: 'true' });
              });
          } else {
            this.status = 'success';
            form.reset();
            this._router.navigate(['/']);
            this.sended.emit({ send: 'true' });
          }
        } else {
          this.status = 'error';
        }
      },
      error => {
        var errorMessage = <any>error;
        console.log(errorMessage);

        if (errorMessage != null) {
          this.status = 'error';
        }
      }
    )
  }

  public filesToUpload: Array<File>;
  fileChangeEvent(fileInput: any) {
    this.filesToUpload = <Array<File>>fileInput.target.files;
  }

  sharePublication(text, userOwn) {
    this._publicationService.sharePublication(this.token, text, userOwn).subscribe(
      response => {
        this.refresh(event);
      },
      error => {
        console.log(<any>error);
      });
  }

}
