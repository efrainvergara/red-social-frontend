/**
  * Propiedad BOSOCIAL 2020
*/

import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params} from '@angular/router';
import { User } from '../../models/user.model';
import { UserService } from '../../services/user.service';
import { UploadService } from '../../services/upload.service';
import { GLOBAL } from '../../services/global';

@Component({
  selector: 'user-portada',
  templateUrl: './user-portada.component.html',
  styleUrls: ['./user-portada.component.css'],
  providers: [UserService, UploadService]
})

export class UserPortadaComponent implements OnInit {

  title: string;
  user: User;
  identity;
  token;
  status: string;
  url:string;

  constructor(
    private _route: ActivatedRoute,
    private _router: Router,
    private _userService: UserService,
    private _uploadService: UploadService
  ) { 
    this.title = 'Actualizar mis datos';
    this.user = this._userService.getIdentity();
    this.identity = this.user;
    this.token = this._userService.getToken();
    this.url = GLOBAL.url;
  }

  ngOnInit(): void {
    console.log(this.user);
    console.log('user-portada.component se ha cargado!!');
  }
  onSubmit(){
    console.log(this.user);
    this._userService.updateUser(this.user).subscribe(
      response => {
        if(response.user == "undefined"){
          this.status = 'error';
        }else{
          this.status = 'success';
          localStorage.setItem('identity', JSON.stringify(this.user));
          this.identity = this.user;

          // Subida imagen de usuario
          this._uploadService.makeFileRequest(this.url+'upload-portada-user/'+this.user._id, [], this.filesToUpload, this.token, 'image')
                             .then((result: any) => {
                               this.user.image = result.user.image;
                               localStorage.setItem('identity', JSON.stringify(this.user));
                             });
        }
      },
      error => {
        var errorMessage = <any>error;
        console.log(errorMessage);

        if(errorMessage != null){
          this.status = 'error';
        }
      }
    )
  }

  filesToUpload: Array<File>;
  fileChangeEvent(fileInput: any){
    this.filesToUpload = <Array<File>>fileInput.target.files;
  }

}
