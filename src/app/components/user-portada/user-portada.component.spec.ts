import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserPortadaComponent } from './user-portada.component';

describe('UserEditComponent', () => {
  let component: UserPortadaComponent;
  let fixture: ComponentFixture<UserPortadaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserPortadaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserPortadaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
