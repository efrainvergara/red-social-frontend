/**
  * Propiedad BOSOCIAL 2020
*/

import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { User } from '../../models/user.model';
import { UserService } from '../../services/user.service';
import * as $ from 'jquery';

@Component({
  selector: 'register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css'],
  providers: [UserService]
})
export class RegisterComponent implements OnInit {

  public title: string;
  public user: User;
  public status: string;

  constructor(
    private _route: ActivatedRoute,
    private _router: Router,
    private _userService: UserService
  ) {
    this.title = 'Registrate';
    this.user = new User(
      "",
      "",
      "",
      "",
      "",
      "",
      "",
      "",
      "",
      "",
      "",
      "",
      "",
      "",
      "",
      "",
      "ROLE_USER",
      "",
      "",
      "",
      "",
      ""
    )
  }

  ngOnInit(): void {
    console.log('Componente registro cargado...');
    $("#customSwitch1").change(function () {
      if ($(this).is(":checked")) {
        $(".Ocultar").show();
      } else {
        $(".Ocultar").hide();
      }
    });

    let state = $('#ciudad');
    let city = $('#localidad');

    state.empty();
    city.empty();

    state.append('<option value="" disabled selected hidden>Departamento</option>');
    state.prop('selectedIndex', 0);

    city.append('<option value="" disabled selected hidden>Ciudad</option>');
    city.prop('selectedIndex', 0);

    // Populate dropdown with list of provinces
    $.getJSON('assets/dataColombia.json', function (data) {
      $.each(data, function (key, entry) {
        state.append($('<option></option>').attr('value', key).text(key));
      })
      $("select.country").change(function () {
        var selectCiudad = $(this).children("option:selected").val();
        city.empty();
        $.each(data[selectCiudad], function (key, entry) {
          city.append($('<option></option>').attr('value', entry).text(entry));
        })
      });
    });

    //jQuery extension method:
    $.fn.filterByText = function (textbox) {
      return this.each(function () {
        var select = '#selectFilter';
        var options = [];
        $(select).find('option').each(function () {
          options.push({
            value: $(this).val(),
            text: $(this).text()
          });
        });
        $(select).data('options', options);

        $(textbox).bind('change keyup', function () {
          var options = $(select).empty().data('options');
          var search = $.trim($(this).val());
          var regex = new RegExp(search, "gi");

          $.each(options, function (i) {
            var option = options[i];
            if (option.text.match(regex) !== null) {
              $(select).append(
                $('<option>').text(option.text).val(option.value)
              );
            }
          });
        });
      });
    };

    // You could use it like this:

    $(function () {
      $('select').filterByText($('#filterSelect'));
    });
  }

  onSubmit(registerForm) {
    this._userService.register(this.user).subscribe(
      response => {
        if (response.user && response.user._id) {
          //console.log(response.user);
          this.status = 'success';
          registerForm.reset();
        } else {
          this.status = 'error';
        }
      },
      error => {
        console.log(<any>error);
      }
    );
  }

}
