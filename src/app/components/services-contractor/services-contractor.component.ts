/**
  * Propiedad BOSOCIAL 2020
*/

import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { UserService } from '../../services/user.service';
import { GLOBAL } from '../../services/global';
import { ServiceService } from '../../services/service.service';
import { Service } from '../../models/service.model';
import { CalendarOptions } from '@fullcalendar/angular';
import esLocale from '@fullcalendar/core/locales/es';

/* jQUERY */
declare var jQuery: any;
declare var $: any;

@Component({
  selector: 'services-contractor',
  templateUrl: './services-contractor.component.html',
  styleUrls: ['./services-contractor.component.css'],
  providers: [UserService, ServiceService]
})
export class ServicesContractorComponent implements OnInit {

  identity;
  token;
  title: string;
  url: string;
  status: string;
  currentDate: any;
  contractors: any[];
  page;
  total: any;
  itemsPerPage: any;
  n:any;
  invitaciones_activas: any;

  constructor(
    private _route: ActivatedRoute,
    private _router: Router,
    private _userService: UserService,
    private _serviceService: ServiceService,
  ) {
    this.title = 'Mis servicios solicitados';
    this.identity = this._userService.getIdentity();
    this.token = this._userService.getToken();
    this.url = GLOBAL.url;
    this.currentDate = Date();
  }

  calendarOptions: CalendarOptions;

  handleDateClick(arg) {
    alert('date click! ' + arg.dateStr)
  }

  handleEventClick(arg) {
    alert('date click! ' + arg.title)
  }

  ngOnInit(): void {
    console.log('services.component cargado correctamente');
    this.providerService();
  }

  // contractorService() {
  //   this._serviceService.getContractorServices(this.token).subscribe(
  //     response => {
  //     },
  //     error => {
  //       var errorMessage = <any>error;
  //       console.log(errorMessage);

  //       if (errorMessage != null) {
  //         this.status = 'error';
  //       }
  //     }
  //   )
  // }


  providerService() {
    this._serviceService.getContractorServices(this.token).subscribe(
      response => {
        //permite que se pinte la data
        this.invitaciones_activas = response.total_items;
        this.contractors = response.contractors;
        this.calendarOptions = {
          locale: esLocale,
          headerToolbar: {
            left: 'prev',
            center: 'title',
            right: 'next,dayGridMonth,timeGridWeek,timeGridDay'
          },
          buttonText: {
            month: ' ',
            week: ' ',
            day: ' '
          },
          buttonIcons: {
            prev: 'far fa-chevron-left',
            next: 'far fa-chevron-right'
          },
          initialView: 'dayGridMonth',
          dateClick: this.handleDateClick.bind(this), // bind is important!
          eventClick: function (event) {
            //alert('date click! ' + event.event.title + " " + event.event.start)
            $('#modalTitle').html(event.event.title);
            $('#modalDate').html(event.event.start.toDateString() + " " + event.event.start.toLocaleTimeString());
            $('#modalDescripcion').html(event.event.extendedProps.description);
            $('#show-name').prepend('<a class="h6 post__author-name fn" href="perfil/' + event.event.extendedProps.userContratante._id + '">' + event.event.extendedProps.userContratante.name + " " + event.event.extendedProps.userContratante.surname + '</a> Creo el Servicio');
            $('#show-image').prepend('<img src="' + GLOBAL.url + 'get-image-user/' + event.event.extendedProps.userContratante.image + '" alt="author">');
            $('#private-event').modal('show');
          },
          events: this.contractors
        };
      },
      error => {
        var errorMessage = <any>error;
        console.log(errorMessage);

        if (errorMessage != null) {
          this.status = 'error';
        }
      }
    )
  }

}
