import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ServicesContractorComponent } from './services-contractor.component';

describe('ServicesContractorComponent', () => {
  let component: ServicesContractorComponent;
  let fixture: ComponentFixture<ServicesContractorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ServicesContractorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ServicesContractorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
