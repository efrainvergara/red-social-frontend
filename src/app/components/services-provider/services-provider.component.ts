/**
  * Propiedad BOSOCIAL 2020
*/

import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { UserService } from '../../services/user.service';
import { GLOBAL } from '../../services/global';
import { ServiceService } from '../../services/service.service';
import { Service } from '../../models/service.model';
import { CalendarOptions } from '@fullcalendar/angular';
import esLocale from '@fullcalendar/core/locales/es';
import { MessageService } from 'src/app/services/message.service';
import { Message } from 'src/app/models/message.model';

/* jQUERY */
declare var jQuery: any;
declare var $: any;

@Component({
  selector: 'services-provider',
  templateUrl: './services-provider.component.html',
  styleUrls: ['./services-provider.component.css'],
  providers: [UserService, ServiceService]
})
export class ServicesProviderComponent implements OnInit {

  identity;
  token;
  title: string;
  url: string;
  status: string;
  currentDate: any;
  providers: any[];
  page;
  total: any;
  itemsPerPage: any;
  n:any;
  invitaciones_activas: any;
  message: Message[];

  constructor(
    private _route: ActivatedRoute,
    private _router: Router,
    private _userService: UserService,
    private _serviceService: ServiceService,
    private _messageService: MessageService,
  ) {
    this.title = 'Mi agenda de trabajo';
    this.identity = this._userService.getIdentity();
    this.token = this._userService.getToken();
    this.url = GLOBAL.url;
    this.currentDate = Date();
  }

  calendarOptions: CalendarOptions;

  handleDateClick(arg) {
    alert('date click! ' + arg.dateStr)
  }

  handleEventClick(arg) {
    alert('date click! ' + arg.title)
  }

  ngOnInit(): void {
    console.log('services.component cargado correctamente');
    this.providerService();
  }

  // contractorService() {
  //   this._serviceService.getContractorServices(this.token).subscribe(
  //     response => {
  //     },
  //     error => {
  //       var errorMessage = <any>error;
  //       console.log(errorMessage);

  //       if (errorMessage != null) {
  //         this.status = 'error';
  //       }
  //     }
  //   )
  // }


  providerService() {
    this._serviceService.getProviderServices(this.token).subscribe(
      response => {
        //permite que se pinte la data
        this.invitaciones_activas = response.total_items;
        this.providers = response.providers;
        this.calendarOptions = {
          locale: esLocale,
          headerToolbar: {
            left: 'prev',
            center: 'title',
            right: 'next,dayGridMonth,timeGridWeek,timeGridDay'
          },
          buttonText: {
            month: ' ',
            week: ' ',
            day: ' '
          },
          buttonIcons: {
            prev: 'far fa-chevron-left',
            next: 'far fa-chevron-right'
          },
          initialView: 'dayGridMonth',
          dateClick: this.handleDateClick.bind(this), // bind is important!
          eventClick: function (event) {
            //alert('date click! ' + event.event.title + " " + event.event.start)
            $('#modalTitle').html(event.event.title);
            $('#modalDate').html(event.event.start.toDateString() + " " + event.event.start.toLocaleTimeString());
            $('#modalDescripcion').html(event.event.extendedProps.description);
            $('#show-name').prepend('<a class="h6 post__author-name fn" href="perfil/' + event.event.extendedProps.userContratista._id + '">' + event.event.extendedProps.userContratista.name + " " + event.event.extendedProps.userContratista.surname + '</a> Creo el Servicio');
            $('#show-image').prepend('<img src="' + GLOBAL.url + 'get-image-user/' + event.event.extendedProps.userContratista.image + '" alt="author">');
            $('#private-event').modal('show');
          },
          events: this.providers
        };
      },
      error => {
        var errorMessage = <any>error;
        console.log(errorMessage);

        if (errorMessage != null) {
          this.status = 'error';
        }
      }
    )
  }

  refresh(event) {
    this.providerService();
  }

  acceptService(id, idOther, comment) {
    this._serviceService.acceptService(this.token, id).subscribe(
      response => {
        this.refresh(event);
        this.sendMessage(idOther, comment)
      },
      error => {
        console.log(<any>error);
      });
  }

  recjetService(id, idOther, comment) {
    this._serviceService.recjetService(this.token, id).subscribe(
      response => {
        this.refresh(event);
        this.sendMessage(idOther, comment)
      },
      error => {
        console.log(<any>error);
      });
  }

  sendMessage(idOther, comment){
    //const comment = "Hola, revisa tus notificaciones, te he enviando una invitación para un servicio.";
    this._messageService.addMessageChat(this.token, comment, idOther).subscribe(
      response => {
        if(response.message){
          this.status = "success";
        }
      },
      error => {
        this.status = "error";
        console.log(<any>error);
      }
    );
  }

}
