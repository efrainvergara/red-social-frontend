import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ServicesProviderComponent } from './services-provider.component';

describe('ServicesComponent', () => {
  let component: ServicesProviderComponent;
  let fixture: ComponentFixture<ServicesProviderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ServicesProviderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ServicesProviderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
