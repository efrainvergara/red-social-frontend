/**
  * Propiedad BOSOCIAL 2020
*/

import { Component, OnInit, Input } from '@angular/core';
import { Router, ActivatedRoute, Params} from '@angular/router';
import { UserService } from '../../services/user.service';
import { GLOBAL } from '../../services/global';
import { PublicationService } from '../../services/publication.service'
import { Publication } from '../../models/publication.model'
import { Validators, FormBuilder } from '@angular/forms';

/* jQUERY */
declare var jQuery:any;
declare var $:any;

@Component({
  selector: 'publications',
  templateUrl: './publications.component.html',
  styleUrls: ['./publications.component.css'],
  providers: [UserService, PublicationService]
})
export class PublicationsComponent implements OnInit {

  identity;
  token;
  title:string;
  url:string;
  status: string;
  page;
  total;
  pages;
  itemsPerPage;
  publications: Publication[];
  @Input() user;
  commentForm: any;
  publication: Publication;

  constructor(
    private _route: ActivatedRoute,
    private _router: Router,
    private _userService: UserService,
    private _publicationService: PublicationService,
    private formBuilder: FormBuilder
  ) {
    this.title = 'Publications';
    this.identity = this._userService.getIdentity();
    this.token = this._userService.getToken();
    this.url = GLOBAL.url;
   }

  ngOnInit(): void {
    console.log('publications.component cargado correctamente')
    this.getPublications(this.user, this.page)
    this.createCommentForm();
  }

  getPublications(user, page, adding = false){
    this._publicationService.getPublicationsUser(this.token, user, page).subscribe(
      response => {
        if(response.publications){
          this.total = response.total_items;
          this.pages = response.pages;
          this.itemsPerPage  = response.items_per_page;

          if(!adding){
            this.publications = response.publications;
          }else{
            var arrayA =  this.publications;
            var arrayB = response.publications;

            this.publications = arrayA.concat(arrayB);

            $("html, body").animate({ scrollTop: $('html').prop("scrollHeight")}, 500);
          }
          
          if(page > this.pages){
            //this._router.navigate(['/home']);
          }
        }else{
          this.status = 'error'
        }
      },
      error => {
        var errorMessage = <any>error;
        console.log(errorMessage);

        if(errorMessage != null){
          this.status = 'error';
        }
      }
    )
  }

  public noMore = false;

  viewMore(){
      this.page += 1;
      if(this.page == this.pages){
          this.noMore = true;
      }

      this.getPublications(this.user, this.page, true);
  }

  deletePublication(id) {
    this._publicationService.deletePublication(this.token, id).subscribe(
      response => {
        this.refresh(event);
      },
      error => {
        console.log(<any>error);
      }
    );
  }
  
  likePublication(id) {
    this._publicationService.likePublication(this.token, id).subscribe(
      response => {
        this.refresh(event);
      },
      error => {
        console.log(<any>error);
      });
  }
  
  refresh(event) {
    this.getPublications(this.user, this.page);
  }

    // Create form for posting comments
    createCommentForm() {
      this.commentForm = this.formBuilder.group({
        comment: ['', Validators.compose([
          Validators.required,
          Validators.minLength(1),
          Validators.maxLength(200)
        ])]
      })
    }

  // Function to post a new comment
  postComment(id) {
    const comment = this.commentForm.get('comment').value; // Get the comment value to pass to service function
    // Function to save the comment to the database
    this._publicationService.commentPublication(this.token,id, comment).subscribe(data => {
      this.getPublications(this.user, this.page); // Refresh all blogs to reflect the new comment
      this.commentForm.reset();
      this.status = 'success';
    }, error => {
      var errorMessage = <any>error;
      console.log(errorMessage);

      if (errorMessage != null) {
        this.status = 'error';
      }
    });
  }

  deleteComment(publication, comment) {
    this._publicationService.deleteComment(this.token, publication, comment).subscribe(
      response => {
        this.refresh(event);
      },
      error => {
        console.log(<any>error);
      }
    );
  }

  updatePublicationSubmit(id) {
    this._publicationService.updatePublication(id, this.publication, this.token).subscribe(
      response => {
        this.refresh(event);
      },
      error => {
        console.log(<any>error);
      }
    );
  }

}
