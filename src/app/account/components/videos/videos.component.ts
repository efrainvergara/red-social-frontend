/**
  * Propiedad BOSOCIAL 2020
*/

import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { User } from '../../../models/user.model';
import { Follow } from '../../../models/follow.model';
import { UserService } from '../../../services/user.service';
import { Service } from '../../../models/service.model';
import { FollowService } from '../../../services/follow.service';
import { ServiceService } from '../../../services/service.service';
import { GLOBAL } from '../../../services/global';
import { CalendarOptions } from '@fullcalendar/angular';
import esLocale from '@fullcalendar/core/locales/es';
import { PublicationService } from 'src/app/services/publication.service';

/* jQUERY */
declare var jQuery: any;
declare var $: any;

@Component({
  selector: 'app-videos',
  templateUrl: './videos.component.html',
  styleUrls: ['./videos.component.css'],
  providers: [UserService, FollowService, ServiceService, PublicationService]
})
export class VideosComponent implements OnInit {

  title: string;
  user: User;
  status: string;
  identity;
  token;
  stats;
  url;
  followed;
  following;
  service: Service;
  providers: any[];
  publications: any[];

  constructor(
    private _route: ActivatedRoute,
    private _router: Router,
    private _userService: UserService,
    private _followService: FollowService, 
    private _serviceService: ServiceService,
    private _publicationService: PublicationService,
  ) { 
    this.title = 'Perfil';
    this.identity = this._userService.getIdentity();
    this.token = this._userService.getToken();
    this.url = GLOBAL.url;
    this.followed = false;
    this.following = false;
    this.service = new Service("", "", "", "", "", "", "espera", "#f39c12", "", "", "");
    }

  ngOnInit(): void {
    console.log('profile.component cargado correctamente');
    this.loadPage();
  }

  loadPage(){
    this._route.params.subscribe(params => {
      let id = params['id'];
      this.getUser(id);
      this.getCounters(id);
      this.providerService(id);
      this.getPublications(id, 1);
    })
  }

  getUser(id){
    this._userService.getUser(id).subscribe(
      response => {
        if(response.user){
          this.user = response.user;
          if(response.following && response.following._id){
            this.following = true;
          }else{
            this.following= false;
          }

          if(response.followed && response.followed._id){
            this.followed = true
          }else{
            this.followed =- false
          }
        }else{
          this.status = 'error';
        }
      },
      error => {
        console.log(<any>error);
        this._router.navigate(['/perfil', this.identity._id]);
      }
    );
  }

  getCounters(id){
    this._userService.getCounters(id).subscribe(
      response => {
        console.log(response);
        this.stats = response;
      },
      error => {
        console.log(<any>error);
      }
    )
  }

  followUser(followed){
    var follow = new Follow('', this.identity._id, followed);

    this._followService.addFollow(this.token, follow).subscribe(
      response => {
        this.following = true;
      },
      error => {
        console.log(<any>error);
      }
    );
  }

  unfollowUser(followed){
    this._followService.deleteFollow(this.token, followed).subscribe(
      response => {
        this.following = false;
      },
      error => {
        console.log(<any>error);
      }
    )
  }

  public followUserOver;
  mouseEnter(user_id){
    this.followUserOver = user_id;
  }

  mouseLeave(){
    this.followUserOver = 0;
  }

  onSubmit(serviceForm, id){
    this._serviceService.addService(this.token, this.service, id).subscribe(
      response => {
          this.status = 'success';
          serviceForm.reset();
      },
      error => {
        console.log(<any>error);
        this.status = 'error';
      }
    );
  }

  calendarOptions: CalendarOptions;

  handleDateClick(arg) {
    alert('date click! ' + arg.dateStr)
  }

  handleEventClick(arg) {
    alert('date click! ' + arg.title)
  }

  providerService(id) {
    this._serviceService.getProviderServicesWithId(id ,this.token).subscribe(
      response => {
        //permite que se pinte la data
        this.providers = response.providers;
        this.calendarOptions = {
          locale: esLocale,
          headerToolbar: {
            left: 'prev',
            center: 'title',
            right: 'next,dayGridMonth,timeGridWeek,timeGridDay'
          },
          buttonText: {
            month: ' ',
            week: ' ',
            day: ' '
          },
          buttonIcons: {
            prev: 'far fa-chevron-left',
            next: 'far fa-chevron-right'
          },
          initialView: 'dayGridMonth',
          dateClick: this.handleDateClick.bind(this), // bind is important!
          eventClick: function (event) {
            //alert('date click! ' + event.event.title + " " + event.event.start)
            $('#modalTitle').html(event.event.title);
            $('#modalDate').html(event.event.start.toDateString() + " " + event.event.start.toLocaleTimeString());
            $('#modalDescripcion').html(event.event.extendedProps.description);
            $('#show-name').prepend('<a class="h6 post__author-name fn" href="perfil/' + event.event.extendedProps.userContratatista + '">' + event.event.extendedProps.userContratatista.name + " " + event.event.extendedProps.userContratatista.surname + '</a> Creo el Servicio');
            $('#show-image').prepend('<img src="' + GLOBAL.url + 'get-image-user/' + event.event.extendedProps.userContratatista.image + '" alt="author">');
            $('#private-event').modal('show');
          },
          events: this.providers
        };
      },
      error => {
        var errorMessage = <any>error;
        console.log(errorMessage);

        if (errorMessage != null) {
          this.status = 'error';
        }
      }
    )
  }

  getPublications(user, page, adding = false){
    this._publicationService.getPublicationsUser(this.token, user, page).subscribe(
      response => {
        this.publications = response.publications
      },
      error => {
        var errorMessage = <any>error;
        console.log(errorMessage);

        if(errorMessage != null){
          this.status = 'error';
        }
      }
    )
  }

}

