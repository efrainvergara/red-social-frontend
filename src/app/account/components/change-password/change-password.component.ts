/**
  * Propiedad BOSOCIAL 2020
*/

import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { User } from '../../../models/user.model';
import { UserService } from '../../../services/user.service';
import { UploadService } from '../../../services/upload.service';
import { GLOBAL } from '../../../services/global';

@Component({
  selector: 'change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.css'],
  providers: [UserService, UploadService]
})

export class ChangePasswordComponent implements OnInit {

  title: string;
  user: User;
  identity;
  token;
  status: string;
  url: string;
  statusTrue: any;
  statusFalse: any;

  constructor(
    private _route: ActivatedRoute,
    private _router: Router,
    private _userService: UserService,
    private _uploadService: UploadService
  ) {
    this.title = 'Cambiar Contraseña';
    this.user = this._userService.getIdentity();
    this.identity = this.user;
    this.token = this._userService.getToken();
    this.url = GLOBAL.url;
  }

  ngOnInit(): void {
    console.log(this.user);
    console.log('change-password.component se ha cargado!!');
  }
  onSubmit(id) {
    this._userService.updatePassword(id, this.user).subscribe(
      response => {
        this.statusTrue = response.message;
        /*setTimeout(function () {
          localStorage.clear();
          this.identity = null;
          this._router.navigate(['/login']);
          this._router.navigate(['/login']);
        }, 5000);*/
      },
      error => {
        this.statusFalse = "Uppsss, hubo un problema al cambiar la contraseña.";
      }
    )
  }
}
