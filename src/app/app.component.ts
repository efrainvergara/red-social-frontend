/**
  * Propiedad BOSOCIAL 2020
*/

import { Component, OnInit, DoCheck } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router'
import { UserService } from './services/user.service'
import { GLOBAL } from './services/global'
import { User } from './models/user.model'
import { ChatAdapter } from 'ng-chat';
import { DemoAdapter } from '../app/demo-adapters';
import { MessageService } from './services/message.service';
import { Message } from './models/message.model';
import { FormBuilder, Validators } from '@angular/forms';
import { ServiceService } from './services/service.service';

/* jQUERY */
declare var jQuery: any;
declare var $: any;


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [UserService, MessageService, ServiceService]
})
export class AppComponent implements OnInit, DoCheck {
  title: string;
  identity;
  url: string;
  results: any[];
  status: string;
  term: string;
  users: User[];
  word: any;
  token;
  messages: Message[];
  pages;
  total;

  userId = 999;

  public adapter: ChatAdapter = new DemoAdapter();
  commentForm: any;
  commentFormChat: any;
  messagesNotification: Message[];
  totalN;
  invitaciones_activas: any;
  providers: any[];
  invitaciones_activasN: any;
  providersN: any[];

  constructor(
    private _userService: UserService,
    private _messageService: MessageService,
    private _serviceService: ServiceService,
    private _route: ActivatedRoute,
    private _router: Router,
    private formBuilder: FormBuilder
  ) {
    this.title = 'Ng Social';
    this.url = GLOBAL.url;
    this.term;
    this.loadScripts();
    this.identity = this._userService.getIdentity();
    this.token = this._userService.getToken();
  }

  albumSelected: any;

  ngOnInit() {
    this.identity = this._userService.getIdentity();
    this.getUsers();
    this.createCommentForm();
    this.getmessages(this.token, 1);
    this.providerService();
  }

  loadScripts() {
    const dynamicScripts = [
      'assets/js/main.js'
    ];
    for (let i = 0; i < dynamicScripts.length; i++) {
      const node = document.createElement('script');
      node.src = dynamicScripts[i];
      node.type = 'text/javascript';
      node.async = false;
      node.charset = 'utf-8';
      document.getElementsByTagName('head')[0].appendChild(node);
    }
  }

  ngDoCheck() {
    this.identity = this._userService.getIdentity();
  }

  logout() {
    localStorage.clear();
    this.identity = null;
    this._router.navigate(['/login']);
    window.location.href = "/login";
  }

  getSearch(param) {
    this._userService.searchGlobal(param).subscribe(
      response => {
        this.results = response.results;
        
        this.word = response.word;
      },
      error => {
        var errorMessage = <any>error;
        console.log(errorMessage);

        if (errorMessage != null) {
          this.status = 'error';
        }
      }
    )

  }

  getUsers() {
    this._userService.getUsers(1).subscribe(
      response => {
        this.users = response.users;
      },
      error => {
        var errorMessage = <any>error;
        console.log(errorMessage);

        if (errorMessage != null) {
          this.status = 'error';
        }
      }
    )
  }

  // Create form for posting comments
  createCommentForm() {
    this.commentFormChat = this.formBuilder.group({
      comment: ['', Validators.compose([
        Validators.required,
        Validators.minLength(1),
        Validators.maxLength(200)
      ])]
    })
  }

  postCommentChat(id){
    const comment = this.commentFormChat.get('comment').value; // Get the comment value to pass to service function
    this._messageService.addMessageChat(this.token, comment, id).subscribe(
      response => {
        if(response.message){
          this.status = "success";
          this.getMessagesChat(1, id)
          this.commentFormChat.reset();
        }
      },
      error => {
        this.status = "error";
        console.log(<any>error);
      }
    );
  }

  getMessagesChat(page, id) {
    this._messageService.getChatMessages(page, id).subscribe(
      response => {
        if (!response.messages) {
          
        }else{
          this.messages = response.messages;
          this.total = response.total;
          this.pages = response.pages;
        }
      }, error => {
        console.log(<any>error)
      }
    )
  }

  getmessages(token, page) {
    this._messageService.getMyMessages(token, page).subscribe(
      response => {
        if (!response.messages) {
          
        }else{
          this.messagesNotification = response.messages;
          this.totalN = response.total;
        }
      }, error => {
        console.log(<any>error)
      }
    )
  }

  providerService() {
    this._serviceService.getProviderServices(this.token).subscribe(
      response => {
        //permite que se pinte la data
        this.invitaciones_activasN = response.total_items;
        this.providersN = response.providers;
      },
      error => {
        var errorMessage = <any>error;
        console.log(errorMessage);

        if (errorMessage != null) {
          this.status = 'error';
        }
      }
    )
  }
}
