/**
  * Propiedad BOSOCIAL 2020
*/

import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http'
import { Observable } from 'rxjs/Observable';
import { GLOBAL } from './global';
import { Message } from '../models/message.model';

@Injectable()
export class MessageService {
    url: string;
    token: any;

    constructor(private _http: HttpClient) {
        this.url = GLOBAL.url;
    }

    getToken(){
        let token = JSON.parse(localStorage.getItem('token'));

        if(token != "undefined"){
            this.token = token;
        }else{
            this.token = null;
        }

        return this.token;
    }

    addMessage(token, message): Observable<any> {
        let params = JSON.stringify(message);
        let headers = new HttpHeaders().set('Content-Type', 'application/json')
            .set('Authorization', token);

        return this._http.post(this.url + 'message', params, { headers: headers });
    }

    addMessageChat(token, message, id): Observable<any> {
        let params = JSON.stringify(message);
        const blogData = {
            receiver: id,
            text: message
          }
        let headers = new HttpHeaders().set('Content-Type', 'application/json')
            .set('Authorization', token);

        return this._http.post(this.url + 'message', blogData, { headers: headers });
    }

    getMyMessages(token, page = 1): Observable<any> {
        let headers = new HttpHeaders().set('Content-Type', 'application/json')
            .set('Authorization', token);

        return this._http.get(this.url + 'my-messages/'+page, { headers: headers });
    }

    getEmmitMessages(token, page = 1): Observable<any> {
        let headers = new HttpHeaders().set('Content-Type', 'application/json')
            .set('Authorization', token);

        return this._http.get(this.url + 'messages/'+page, { headers: headers });
    }

    getChatMessages(page = 1, id): Observable<any> {
        let headers = new HttpHeaders().set('Content-Type', 'application/json')
            .set('Authorization', this.getToken());

        return this._http.get(this.url + 'chat-messages/' + id + '/' +page, { headers: headers });
    }

}