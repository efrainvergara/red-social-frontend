/**
  * Propiedad BOSOCIAL 2020
*/

import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { GLOBAL } from './global';

@Injectable()
export class AuthService {
  url: any;

  constructor(public _http: HttpClient) {
    this.url = GLOBAL.url;
  }

  requestReset(body): Observable<any> {
    return this._http.post(this.url+'req-reset-password', body);
  }

  newPassword(body): Observable<any> {
    return this._http.post(this.url+'new-password', body);
  }

  ValidPasswordToken(body): Observable<any> {
    return this._http.post(this.url+'valid-password-token', body);
  }
}
