/**
  * Propiedad BOSOCIAL 2020
*/

import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http'
import { Observable } from 'rxjs/Observable';
import { GLOBAL } from './global';
import { User } from '../models/user.model';

@Injectable()
export class ServiceService{
    public url:string;

    constructor(public _http: HttpClient){
        this.url = GLOBAL.url;
    }

    addService(token, service, id):Observable<any>{
        var params = JSON.stringify(service);
        let headers = new HttpHeaders().set('Content-Type','application/json')
                                       .set('Authorization', token);

        return this._http.post(this.url+'service-create/' + id, params, {headers: headers});
    }

    getContractorServices(token):Observable<any>{
        let headers = new HttpHeaders().set('Content-Type','application/json')
                                       .set('Authorization', token);

        return this._http.get(this.url + 'contractor', {headers: headers});
    }

    getProviderServices(token):Observable<any>{
        let headers = new HttpHeaders().set('Content-Type','application/json')
                                       .set('Authorization', token);

        return this._http.get(this.url + 'provider', {headers: headers});
    }

    getProviderServicesWithId(id, token):Observable<any>{
        let headers = new HttpHeaders().set('Content-Type','application/json')
                                       .set('Authorization', token);

        return this._http.get(this.url + 'date-provider/' + id, {headers: headers});
    }

    acceptService(token, id): Observable<any>{
        let headers = new HttpHeaders().set('Content-Type','application/json')
                                       .set('Authorization', token);
        
        return this._http.put(this.url+'accept-service/'+id, '', {headers: headers})
    }

    recjetService(token, id): Observable<any>{
        let headers = new HttpHeaders().set('Content-Type','application/json')
                                       .set('Authorization', token);
        
        return this._http.put(this.url+'recjet-service/'+id, '', {headers: headers})
    }
}
