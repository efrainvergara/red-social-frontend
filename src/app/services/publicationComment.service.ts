/**
  * Propiedad BOSOCIAL 2020
*/

import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http'
import { Observable } from 'rxjs/Observable';
import { GLOBAL } from './global';
import { Publication } from '../models/publication.model';
import { PublicationComment } from '../models/publicationComment.model';

@Injectable()
export class PublicationCommentService{
    url:string;

    constructor(private _http: HttpClient){
        this.url = GLOBAL.url;
    }

    addComment(token, publicationComment, id):Observable<any>{
        var params = JSON.stringify(publicationComment);
        let headers = new HttpHeaders().set('Content-Type','application/json')
                                       .set('Authorization', token);

        return this._http.post(this.url+'publication-comment/'+id, params, {headers: headers});
    }

    getCommentsPublication(token, id, page = 1):Observable<any>{
        let headers = new HttpHeaders().set('Content-Type','application/json')
                                       .set('Authorization', token);

        return this._http.get(this.url+'publications-comment/'+ id + '/' + page, {headers: headers});
    }

    getPublicationsUser(token, user_id, page = 1):Observable<any>{
        let headers = new HttpHeaders().set('Content-Type','application/json')
                                       .set('Authorization', token);

        return this._http.get(this.url+'publications-user/'+user_id+'/'+page, {headers: headers});
    }

    deletePublication(token, id){
        let headers = new HttpHeaders().set('Content-Type','application/json')
                                       .set('Authorization', token);

        return this._http.delete(this.url+'publication/'+id, {headers: headers});
    }

    likePublication(token, id): Observable<any>{
        let headers = new HttpHeaders().set('Content-Type','application/json')
                                       .set('Authorization', token);
        
        return this._http.put(this.url+'publication-like/'+id, '', {headers: headers})
    }
}