/**
  * Propiedad BOSOCIAL 2020
*/

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

//Componentes
import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/register/register.component';
import { HomeComponent } from './components/home/home.component';
import { UserEditComponent } from './components/user-edit/user-edit.component';
import { UsersComponent } from './components/users/users.component';
import { TimelineComponent } from './components/timeline/timeline.component';
import { ProfileComponent } from './components/profile/profile.component';
import { FollowingComponent } from './components/following/following.component';
import { FollowedComponent } from './components/followed/followed.component';
import { ServicesProviderComponent } from './components/services-provider/services-provider.component';
import { ServicesContractorComponent } from './components/services-contractor/services-contractor.component';
import { PersonalInformationComponent } from './account/components/personal-information/personal-information.component';
import { SettingsComponent } from './account/components/settings/settings.component';
import { ChangePasswordComponent } from './account/components/change-password/change-password.component';
import { PersonalContabilityComponent } from './contability/components/personal-contability/personal-contability.component';
import { SearchComponent } from './components/search/search.component';
import { PhotosComponent } from './account/components/photos/photos.component';
import { VideosComponent } from './account/components/videos/videos.component';
// Mensajes
import { MainComponent } from './messages/components/main/main.component';
import { AddComponent } from './messages/components/add/add.component';
import { ReceivedComponent } from './messages/components/received/received.component';
import { SendedComponent } from './messages/components/sended/sended.component';

import { UserGuard } from './services/user.guard';
import { ResponseResetComponent } from './components/response-reset/response-reset.component';

const routes: Routes = [
  { path: '', component: TimelineComponent, canActivate: [UserGuard] },
  { path: 'home', component: HomeComponent },
  { path: 'login', component: LoginComponent },
  { path: 'register', component: RegisterComponent },
  { path: 'mis-datos', component: UserEditComponent, canActivate: [UserGuard] },
  { path: 'gente', component: UsersComponent, canActivate: [UserGuard] },
  { path: 'gente/:page', component: UsersComponent, canActivate: [UserGuard] },
  { path: 'timeline', component: TimelineComponent, canActivate: [UserGuard] },
  { path: 'perfil/:id', component: ProfileComponent, canActivate: [UserGuard] },
  { path: 'siguiendo/:id/:page', component: FollowingComponent, canActivate: [UserGuard] },
  { path: 'seguidores/:id/:page', component: FollowedComponent, canActivate: [UserGuard] },
  { path: 'services-provider', component: ServicesProviderComponent, canActivate: [UserGuard] },
  { path: 'services-contractor', component: ServicesContractorComponent, canActivate: [UserGuard] },
  { path: 'personal-information', component: PersonalInformationComponent, canActivate: [UserGuard] },
  { path: 'settings-account', component: SettingsComponent, canActivate: [UserGuard] },
  { path: 'change-password', component: ChangePasswordComponent, canActivate: [UserGuard] },
  { path: 'contabilidad', component: PersonalContabilityComponent, canActivate: [UserGuard] },
  { path: 'search/:id', component: SearchComponent, canActivate: [UserGuard] },
  { path: 'fotos/:id', component: PhotosComponent, canActivate: [UserGuard] },
  { path: 'videos/:id', component: VideosComponent, canActivate: [UserGuard] },
  //Mensajes
  { path: 'mensajes/enviar', component: AddComponent, canActivate: [UserGuard] },
  { path: 'mensajes/recibidos', component: ReceivedComponent, canActivate: [UserGuard] },
  { path: 'mensajes/enviados', component: SendedComponent, canActivate: [UserGuard] },
  { path: 'mensajes/enviados/:page', component: SendedComponent, canActivate: [UserGuard] },
  { path: 'mensajes/recibidos/:page', component: ReceivedComponent, canActivate: [UserGuard] },
  // Recuperar contraseña con token
  { path: 'response-reset-password/:token', component: ResponseResetComponent},
  { path: '**', component: TimelineComponent, canActivate: [UserGuard] },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
