/**
  * Propiedad BOSOCIAL 2020
*/

import { BrowserModule } from '@angular/platform-browser';
import { NgModule, LOCALE_ID } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { MomentModule } from 'angular2-moment';
import localeEs from '@angular/common/locales/es';
import { CommonModule } from '@angular/common';

//Componentes
import { AppComponent } from './app.component';
import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/register/register.component';
import { HomeComponent } from './components/home/home.component';
import { UserEditComponent } from './components/user-edit/user-edit.component';
import { UserPortadaComponent } from './components/user-portada/user-portada.component';
import { UsersComponent } from './components/users/users.component';
import { SidebarComponent } from './components/sidebar/sidebar.component';
import { TimelineComponent } from './components/timeline/timeline.component';
import { PublicationsComponent } from './components/publications/publications.component';
import { ProfileComponent } from './components/profile/profile.component';
import { FollowingComponent } from './components/following/following.component';
import { FollowedComponent } from './components/followed/followed.component';
import { ServicesProviderComponent } from './components/services-provider/services-provider.component';
import { ServicesContractorComponent } from './components/services-contractor/services-contractor.component';

//Servicios
import { UserService } from './services/user.service';
import { UserGuard } from './services/user.guard';
import { registerLocaleData } from '@angular/common';

//fullcalendar
import { FullCalendarModule } from '@fullcalendar/angular'; // the main connector. must go first
import dayGridPlugin from '@fullcalendar/daygrid'; // a plugin
import interactionPlugin from '@fullcalendar/interaction';
import timeGridPlugin from '@fullcalendar/timegrid';

//account component
import { PersonalInformationComponent } from './account/components/personal-information/personal-information.component';
import { SettingsComponent } from './account/components/settings/settings.component';
import { ChangePasswordComponent } from './account/components/change-password/change-password.component';
import { PhotosComponent } from './account/components/photos/photos.component';
import { VideosComponent } from './account/components/videos/videos.component';

//contabilidad
import { PersonalContabilityComponent } from './contability/components/personal-contability/personal-contability.component';

//search
import { SearchComponent } from './components/search/search.component';
import { HeaderProfileComponent } from './components/header-profile/header-profile.component';

//Chat
import { NgChatModule } from 'ng-chat';

// message
import { MainComponent } from './messages/components/main/main.component';
import { AddComponent } from './messages/components/add/add.component';
import { ReceivedComponent } from './messages/components/received/received.component';
import { SendedComponent } from './messages/components/sended/sended.component';
import { ResponseResetComponent } from './components/response-reset/response-reset.component';

FullCalendarModule.registerPlugins([ // register FullCalendar plugins
  dayGridPlugin,
  interactionPlugin,
  timeGridPlugin
]);

//Pipes in spanish

registerLocaleData(localeEs, 'es');

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegisterComponent,
    HomeComponent,
    UserEditComponent,
    UserPortadaComponent,
    UsersComponent,
    SidebarComponent,
    TimelineComponent,
    PublicationsComponent,
    ProfileComponent,
    FollowingComponent,
    FollowedComponent,
    ServicesProviderComponent,
    ServicesContractorComponent,
    PersonalInformationComponent,
    SettingsComponent,
    ChangePasswordComponent,
    PersonalContabilityComponent,
    SearchComponent,
    HeaderProfileComponent,
    PhotosComponent,
    VideosComponent,
    MainComponent,
    AddComponent,
    ReceivedComponent,
    SendedComponent,
    ResponseResetComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    MomentModule,
    ReactiveFormsModule,
    FullCalendarModule,
    NgChatModule,
    CommonModule
  ],
  providers: [UserService, UserGuard, { provide: LOCALE_ID, useValue: 'es' }],
  bootstrap: [AppComponent]
})
export class AppModule { }
