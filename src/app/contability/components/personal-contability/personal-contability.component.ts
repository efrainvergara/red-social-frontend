/**
  * Propiedad BOSOCIAL 2020
*/

import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { GLOBAL } from 'src/app/services/global';
import { UserService } from './../../../services/user.service';
import { ServiceService } from 'src/app/services/service.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Chart } from 'chart.js';

/* jQUERY */
declare var jQuery: any;
declare var $: any;


@Component({
  selector: 'app-personal-contability',
  templateUrl: './personal-contability.component.html',
  styleUrls: ['./personal-contability.component.css'],
  providers: [UserService, ServiceService]
})
export class PersonalContabilityComponent implements OnInit, AfterViewInit {

  @ViewChild('barCanvas') barCanvas;
  @ViewChild('doughnutCanvas') doughnutCanvas;
  @ViewChild('lineCanvas') lineCanvas;

  providers: any[];
  title: string;
  identity: any;
  token: any;
  url: any;
  currentDate: string;
  status: string;
  contractors: any[];
  total: any;

  barChart: any;
  doughnutChart: any;
  lineChart: any;

  constructor(
    private _route: ActivatedRoute,
    private _router: Router,
    private _userService: UserService,
    private _serviceService: ServiceService,
  ) {
    this.title = 'Cree y maneje todos sus eventos';
    this.token = this._userService.getToken();
    this.url = GLOBAL.url;
    this.currentDate = Date();
    this.loadScripts();
    this.loadStyles();
  }
  ngAfterViewInit(): void {
    this.barChartMethod();
    this.doughnutChartMethod();
    this.lineChartMethod();
  }

  ngOnInit(): void {
    this.contractorService();
    this.providerService();

    $(function () {
      $('a.profile-tab').click(function () {
        $('table#table').bootstrapTable('resetView');
      });
    });

    $(function () {
      $('a.home-tab').click(function () {
        $('table#table2').bootstrapTable('resetView');
      });
    });

    $(function () {
      var $table = $('#table');
      $('#export-json').click(function () {
        $table.tableExport({
          type: 'json',
          escape: false
        });
      });
      $('#export-xml').click(function () {
        $table.tableExport({
          type: 'xml',
          escape: false
        });
      });
      $('#export-csv').click(function () {
        $table.tableExport({
          type: 'csv',
          escape: false
        });
      });
      $('#export-json').click(function () {
        $table.tableExport({
          type: 'json',
          escape: false
        });
      });
      $('#export-txt').click(function () {
        $table.tableExport({
          type: 'txt',
          escape: false
        });
      });
      $('#export-sql').click(function () {
        $table.tableExport({
          type: 'sql',
          escape: false
        });
      });
      $('#export-excel').click(function () {
        $table.tableExport({
          type: 'excel',
          escape: false
        });
      });
      $('#export-pdf').click(function () {
        $table.tableExport({
          type: 'pdf',
          escape: false
        });
      });
    });

    $(function () {
      var $table = $('#table2');
      $('#export-json1').click(function () {
        $table.tableExport({
          type: 'json',
          escape: false
        });
      });
      $('#export-xml1').click(function () {
        $table.tableExport({
          type: 'xml',
          escape: false
        });
      });
      $('#export-csv1').click(function () {
        $table.tableExport({
          type: 'csv',
          escape: false
        });
      });
      $('#export-json1').click(function () {
        $table.tableExport({
          type: 'json',
          escape: false
        });
      });
      $('#export-txt1').click(function () {
        $table.tableExport({
          type: 'txt',
          escape: false
        });
      });
      $('#export-sql1').click(function () {
        $table.tableExport({
          type: 'sql',
          escape: false
        });
      });
      $('#export-excel1').click(function () {
        $table.tableExport({
          type: 'excel',
          escape: false
        });
      });
      $('#export-pdf1').click(function () {
        $table.tableExport({
          type: 'pdf',
          escape: false
        });
      });
    });
  }

  loadScripts() {
    const dynamicScripts = [
      'https://cdnjs.cloudflare.com/ajax/libs/bootstrap-table/1.10.0/bootstrap-table.js',
      'https://cdnjs.cloudflare.com/ajax/libs/bootstrap-table/1.9.1/extensions/editable/bootstrap-table-editable.js',
      'https://cdnjs.cloudflare.com/ajax/libs/bootstrap-table/1.9.1/extensions/export/bootstrap-table-export.js',
      'https://unpkg.com/tableexport.jquery.plugin/tableExport.min.js',
      'https://unpkg.com/tableexport.jquery.plugin/libs/jsPDF/jspdf.min.js',
      'https://unpkg.com/tableexport.jquery.plugin/libs/jsPDF-AutoTable/jspdf.plugin.autotable.js',
      'https://cdnjs.cloudflare.com/ajax/libs/bootstrap-table/1.9.1/extensions/filter-control/bootstrap-table-filter-control.js',
      'assets/js/libs/Chart.js',
      'assets/js/libs/chartjs-plugin-deferred.js'
    ];
    for (let i = 0; i < dynamicScripts.length; i++) {
      const node = document.createElement('script');
      node.src = dynamicScripts[i];
      node.type = 'text/javascript';
      node.async = false;
      node.charset = 'utf-8';
      document.getElementsByTagName('head')[0].appendChild(node);
    }
  }

  loadStyles() {
    const dynamicStyles = [
      'https://cdnjs.cloudflare.com/ajax/libs/bootstrap-table/1.10.0/bootstrap-table.min.css',
      'https://rawgit.com/vitalets/x-editable/master/dist/bootstrap3-editable/css/bootstrap-editable.css',
    ];
    for (let i = 0; i < dynamicStyles.length; i++) {
      const node = document.createElement('link');
      node.href = dynamicStyles[i];
      node.rel = 'stylesheet';
      node.charset = 'utf-8';
      document.getElementsByTagName('head')[0].appendChild(node);
    }
  }

  providerService() {
    this._serviceService.getProviderServices(this.token).subscribe(
      response => {
        //permite que se pinte la data
        this.providers = response.providers;

        var month = new Array();
        month[0] = "January";
        month[1] = "February";
        month[2] = "March";
        month[3] = "April";
        month[4] = "May";
        month[5] = "June";
        month[6] = "July";
        month[7] = "August";
        month[8] = "September";
        month[9] = "October";
        month[10] = "November";
        month[11] = "December";

        for (let item of this.providers) {
          let hola = new Date(item.date)
          
          var n = month[hola.getMonth()];
        }

        this.total += response.cost;

        this.lineChart = new Chart(this.lineCanvas.nativeElement, {
          type: 'line',
          data: {
            labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'November', 'December'],
            datasets: [
              {
                label: 'Sell per week',
                fill: false,
                lineTension: 0.1,
                backgroundColor: 'rgba(75,192,192,0.4)',
                borderColor: 'rgba(75,192,192,1)',
                borderCapStyle: 'butt',
                borderDash: [],
                borderDashOffset: 0.0,
                borderJoinStyle: 'miter',
                pointBorderColor: 'rgba(75,192,192,1)',
                pointBackgroundColor: '#fff',
                pointBorderWidth: 1,
                pointHoverRadius: 5,
                pointHoverBackgroundColor: 'rgba(75,192,192,1)',
                pointHoverBorderColor: 'rgba(220,220,220,1)',
                pointHoverBorderWidth: 2,
                pointRadius: 1,
                pointHitRadius: 10,
                data: n,
                spanGaps: false,
              }
            ]
          }
        });
      },
      error => {
        var errorMessage = <any>error;
        console.log(errorMessage);

        if (errorMessage != null) {
          this.status = 'error';
        }
      }
    )
  }

  contractorService() {
    this._serviceService.getContractorServices(this.token).subscribe(
      response => {
        //permite que se pinte la data
        this.contractors = response.contractors;
      },
      error => {
        var errorMessage = <any>error;
        console.log(errorMessage);

        if (errorMessage != null) {
          this.status = 'error';
        }
      }
    )
  }

  barChartMethod() {
    this.barChart = new Chart(this.barCanvas.nativeElement, {
      type: 'bar',
      data: {
        labels: ['BJP', 'INC', 'AAP', 'CPI', 'CPI-M', 'NCP'],
        datasets: [{
          label: '# of Votes',
          data: [200, 50, 30, 15, 20, 34],
          backgroundColor: [
            'rgba(255, 99, 132, 0.2)',
            'rgba(54, 162, 235, 0.2)',
            'rgba(255, 206, 86, 0.2)',
            'rgba(75, 192, 192, 0.2)',
            'rgba(153, 102, 255, 0.2)',
            'rgba(255, 159, 64, 0.2)'
          ],
          borderColor: [
            'rgba(255,99,132,1)',
            'rgba(54, 162, 235, 1)',
            'rgba(255, 206, 86, 1)',
            'rgba(75, 192, 192, 1)',
            'rgba(153, 102, 255, 1)',
            'rgba(255, 159, 64, 1)'
          ],
          borderWidth: 1
        }]
      },
      options: {
        scales: {
          yAxes: [{
            ticks: {
              beginAtZero: true
            }
          }]
        }
      }
    });
  }

  doughnutChartMethod() {
    this.doughnutChart = new Chart(this.doughnutCanvas.nativeElement, {
      type: 'doughnut',
      data: {
        labels: ['BJP', 'Congress', 'AAP', 'CPM', 'SP'],
        datasets: [{
          label: '# of Votes',
          data: [50, 29, 15, 10, 7],
          backgroundColor: [
            'rgba(255, 159, 64, 0.2)',
            'rgba(255, 99, 132, 0.2)',
            'rgba(54, 162, 235, 0.2)',
            'rgba(255, 206, 86, 0.2)',
            'rgba(75, 192, 192, 0.2)'
          ],
          hoverBackgroundColor: [
            '#FFCE56',
            '#FF6384',
            '#36A2EB',
            '#FFCE56',
            '#FF6384'
          ]
        }]
      }
    });
  }

  lineChartMethod() {

  }

}
