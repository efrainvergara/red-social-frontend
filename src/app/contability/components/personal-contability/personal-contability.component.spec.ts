import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PersonalContabilityComponent } from './personal-contability.component';

describe('PersonalContabilityComponent', () => {
  let component: PersonalContabilityComponent;
  let fixture: ComponentFixture<PersonalContabilityComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PersonalContabilityComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PersonalContabilityComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
